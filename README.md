## Latin - An Automatic Item Generator for Latin mastery

This repo contains functions and data permitting the creation of a number of test items for Latin grammar.

(No intentions of extending this to syntax for the time being)

Noun and Verb data are R data.frames that contain basic grammar info on the nouns and verbs of the Latin curriculum 
of Greek High School Humanities Orientation.
(not commited yet)

AVC and ANC (standing for Advanced Verb/Noun Conjugator, respectively) operate on any row of these data.frames
(or accept an equivalent manual vector instead) and expand the whole grammar table. (Only regular cases for the time being)

This allows for other functions to sample items from these tables according to a set of constraints. (Automatic ITem Generation - not commited yet)

In the future, methods for establishing an estimate of the difficulty of the items for various levels of student ability/achievement will be implemented.

One possible method is a linear model relating item construction characteristics to estimates of difficulty drawn from a sample of students. (Q-matrix)